# README #

Collection of ascii art I made during COVID-19 stay at home time. I like Cowsay and I decided to make my own version of it with more complex ascii images.
I'll put that is another repository.

### What is this repository for? ###

* Ascii art to spice up terminal programs
* Version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

It's just text files. Clone the repo and enjoy. Best when when displayed in a black terminal with white lettering

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Buck Roberts
* Other community or team contact